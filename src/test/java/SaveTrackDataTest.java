import com.es.tictest.model.TrackData;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Created by fezzz on 11.08.2016.
 */
@Test(enabled = true)
public class SaveTrackDataTest extends Arquillian {
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap
            .create(JavaArchive.class)
            .addClasses(TrackData.class)
            .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
            .addAsResource("META-INF/test-persistence.xml",
                "META-INF/persistence.xml");
    }

    @PersistenceContext
    EntityManager em;

    /**
     * тест на вставку записи в базу данных
     */
    @Test
    @Transactional
    public void checkInsert() {
        TrackData trackData = new TrackData(1, 0, 0, 0, 4.0, 0, 0, 5, "GPS", 1393337635000l);
        em.persist(trackData);

        CriteriaQuery<TrackData> criteriaQuery = em.getCriteriaBuilder().createQuery(TrackData.class);
        criteriaQuery.select(criteriaQuery.from(TrackData.class));
        Assert.assertEquals( em.createQuery(criteriaQuery).getResultList().size(), 1);
    }
}
