package com.es.tictest.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Данные
 */
@Entity
@Table(name="track_data")
public class TrackData implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "tracker_id")
    private long trackerId;

    private double latitude;

    private double longitude;

    private double horizontalAccuracy;
    
    private double speed;
    
    private double course;
    
    private double altitude;

    private double verticalAccuracy;
    
    private String actuality;

    @Column(name = "timestamp_f")
    private long timestamp;

    public TrackData(long trackerId, double latitude, double longitude, double horizontalAccuracy, double speed,
                     double course, double altitude, double verticalAccuracy, String actuality, long timestamp) {
        this.trackerId = trackerId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.horizontalAccuracy = horizontalAccuracy;
        this.speed = speed;
        this.course = course;
        this.altitude = altitude;
        this.verticalAccuracy = verticalAccuracy;
        this.actuality = actuality;
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(long trackerId) {
        this.trackerId = trackerId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getHorizontalAccuracy() {
        return horizontalAccuracy;
    }

    public void setHorizontalAccuracy(double horizontalAccuracy) {
        this.horizontalAccuracy = horizontalAccuracy;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getCourse() {
        return course;
    }

    public void setCourse(double course) {
        this.course = course;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getVerticalAccuracy() {
        return verticalAccuracy;
    }

    public void setVerticalAccuracy(double verticalAccuracy) {
        this.verticalAccuracy = verticalAccuracy;
    }

    public String getActuality() {
        return actuality;
    }

    public void setActuality(String actuality) {
        this.actuality = actuality;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
