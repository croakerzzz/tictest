package com.es.tictest.server;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

/**
 * Сервис для отсылки сообщения
 */
@Stateless
public class ProducerService {
    @Inject
    private JMSContext context;

    @Resource(mappedName = "java:jboss/jms/queue/ticketQueue")
    private Queue syncQueue;

    public void sendMessage(String text) {
        context.createProducer().send(syncQueue, text);
    }
}
