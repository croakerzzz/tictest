package com.es.tictest.server;

import com.es.tictest.common.Data;
import com.es.tictest.dao.TrackDataDao;
import com.es.tictest.model.TrackData;
import com.google.gson.Gson;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Получатель сообщений
 */
@MessageDriven(name = "TicketQueueReceiver", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup",
        propertyValue = "java:jboss/jms/queue/ticketQueue"),
    @ActivationConfigProperty(propertyName = "destinationType",
        propertyValue = "javax.jms.Queue")
})
public class TicketQueueReceiver implements MessageListener {
    private static final Logger logger = Logger.getLogger("TicketQueueReceiver");

    @EJB
    TrackDataDao trackDataDao;

    @Override
    public void onMessage(Message message) {
        try {
            final String text = message.getBody(String.class);
            logger.info("Received message: " + text);

            Stream.of(decode(text)).forEach((rec) -> {
                TrackData trackData =
                    new TrackData(rec.trackerId, rec.latitude, rec.longitude, rec.horizontalAccuracy, rec.speed,
                        rec.course, rec.altitude, rec.verticalAccuracy, rec.actuality, rec.timestamp);
                trackDataDao.persist(trackData);
            });
            logger.info("Message saved");
        } catch (JMSException ex) {
            logger.severe(ex.toString());
        }
    }

    private Data[] decode(String text) {
        Gson gson = new Gson();
        return gson.fromJson(text, Data[].class);
    }
}
