package com.es.tictest.dao;

import com.es.tictest.model.TrackData;

import javax.ejb.Stateless;

/**
 * Доступ к TrackData
 */
@Stateless
public class TrackDataDao extends AbstractDao<TrackData> {

}
