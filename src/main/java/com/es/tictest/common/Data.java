package com.es.tictest.common;

/**
 * Для конвертации из json
 */
public class Data {
    public long trackerId;
    public double latitude;
    public double longitude;
    public double horizontalAccuracy;
    public double speed;
    public double course;
    public double altitude;
    public double verticalAccuracy;
    public String actuality;
    public long timestamp;

    public Data(long trackerId, double latitude, double longitude, double horizontalAccuracy, double speed,
                double course,
                double altitude, double verticalAccuracy, String actuality, long timestamp) {
        this.trackerId = trackerId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.horizontalAccuracy = horizontalAccuracy;
        this.speed = speed;
        this.course = course;
        this.altitude = altitude;
        this.verticalAccuracy = verticalAccuracy;
        this.actuality = actuality;
        this.timestamp = timestamp;
    }
}
