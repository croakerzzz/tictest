CREATE TABLE track_data
(
  id numeric(18,0),
  tracker_id numeric(18,0),
  latitude numeric(18,6),
  longitude numeric(18,6)
);

create sequ1ence HIBERNATE_SEQUENCE;
