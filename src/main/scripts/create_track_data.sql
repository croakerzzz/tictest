CREATE TABLE track_data
(
  id                 NUMERIC(18, 0) NOT NULL,
  tracker_id         NUMERIC(18, 0),
  latitude           NUMERIC(18, 6),
  longitude          NUMERIC(18, 6),
  speed              NUMERIC(18, 6),
  course             NUMERIC(18, 6),
  altitude           NUMERIC(18, 6),
  actuality          CHARACTER VARYING(100),
  timestamp_f        NUMERIC(18, 0),
  horizontalaccuracy NUMERIC(18, 6),
  verticalaccuracy   NUMERIC(18, 6),
  CONSTRAINT track_data_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE hibernate_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
